﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    private Rigidbody rb;

    [SerializeField]
    private float force;

    [SerializeField]
    private int damage;

    public void Fire()
    {
        rb.AddForce(transform.forward * force);
    }

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            EnemyController enemy = collision.gameObject.GetComponent<EnemyController>();
            enemy.GetHit(damage);
        } 
    }
}
