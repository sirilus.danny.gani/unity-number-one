﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public int health;
    private GameObject player;

    [SerializeField]
    private float speed;

    public void GetHit(int damage)
    {
        health -= damage;
        Debug.Log("Current health: " + health);

        if (health < 1)
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, speed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Bullet"))
        {
            speed = speed / 2f;
        }
        else if (collision.gameObject.tag.Equals("Player"))
        {
            Destroy(this.gameObject);
        }
    }
}
