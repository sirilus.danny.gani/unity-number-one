﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameInstance : MonoBehaviour
{

    [SerializeField]
    private GameObject enemy;

    void Start()
    {
        StartCoroutine(SwitchToEnemy());
    }

    IEnumerator SwitchToEnemy()
    {
        yield return new WaitForSeconds(6f);

        GameObject.Instantiate(enemy, this.transform.position, transform.rotation);

        yield return new WaitForSeconds(2f);

        Destroy(this.gameObject);
    }
	
}
